# LiveValidator - Plugin DOM

[![build status](https://gitlab.com/LiveValidator/Plugin-DOM/badges/master/build.svg)](https://gitlab.com/LiveValidator/Plugin-DOM/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Plugin-DOM/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Plugin-DOM/commits/master)

Simple code that registers LiveValidator core as a DOM method on input elements.

Find the project [home page and docs](https://livevalidator.gitlab.io/).