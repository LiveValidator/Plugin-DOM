export { ITheme, IOptions, TThemeConstructable } from './src/abstractions';
export { PluginCollection } from './src/collection';
export * from './src/plugin';
