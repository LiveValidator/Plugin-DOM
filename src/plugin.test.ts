import { ITester } from 'livevalidator';
import { ITheme, TThemeConstructable } from './abstractions';
import { Plugin } from './plugin';

/**
 * Helper to get a mock theme for tests
 */
function getThemeMock(): ITheme {
  return {
    clearErrors: jest.fn(),
    markRequired: jest.fn(),
    setErrors: jest.fn(),
    setMissing: jest.fn(),
    unmarkRequired: jest.fn(),
    unsetMissing: jest.fn(),
  };
}

/**
 * Helper to register a theme with a constructor
 * @param theme Theme to register
 */
function getThemeConstructor(theme: ITheme): TThemeConstructable {
  return jest.fn(_ => theme);
}

describe('Construction', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('with empty options', () => {
    const input = document.createElement('input');
    const plugin = new Plugin(input);

    expect(plugin.getOptions()).toEqual({
      live: true,
      locale: 'en-US',
      required: false,
    });

    expect(themeConstructor).toHaveBeenCalledTimes(1);
    expect(themeConstructor).toHaveBeenCalledWith(input);

    expect(themeMock.markRequired).toHaveBeenCalledTimes(0);
    expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(1);

    expect(input.required).toBeFalsy();
  });

  test('with options overriding', () => {
    const input = document.createElement('input');

    // Passing in the exact opposite of the defaults (previous test)
    const plugin = new Plugin(input, {
      live: !Plugin.defaults.live,
      locale: 'af',
      required: !Plugin.defaults.required,
    });

    expect(plugin.getOptions()).toEqual({
      live: false,
      locale: 'af',
      required: true,
    });

    expect(themeConstructor).toHaveBeenCalledTimes(1);
    expect(themeConstructor).toHaveBeenCalledWith(input);

    expect(themeMock.markRequired).toHaveBeenCalledTimes(1);
    expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(0);

    expect(input.required).toBeTruthy();
  });

  test('with default being overridden', () => {
    // The opposite of the normal defaults
    Plugin.defaults = {
      live: false,
      locale: 'he',
      required: true,
      theme: themeConstructor,
    };

    const plugin = new Plugin(document.createElement('input'));

    expect(plugin.getOptions()).toEqual({
      live: false,
      locale: 'he',
      required: true,
    });
  });

  test('with theme missing', () => {
    Plugin.defaults = {};

    const action = () => new Plugin(document.createElement('input'));

    expect(action).toThrow('A theme constructor is required');

    expect(themeConstructor).toHaveBeenCalledTimes(0);
  });

  test('with unset default and passed options', () => {
    Plugin.defaults = {
      theme: themeConstructor,
    };

    const plugin = new Plugin(document.createElement('input'));

    expect(plugin.getOptions()).toEqual({
      live: false,
      locale: 'en-US',
      required: false,
    });

    expect(themeConstructor).toHaveBeenCalledTimes(1);
  });
});

describe('setRequired', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;
  let input: HTMLInputElement;

  beforeAll(() => {
    input = document.createElement('input');
  });

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('when not required', () => {
    const plugin = new Plugin(input, { required: false });

    expect(input.required).toBeFalsy();
    expect(plugin.getOptions().required).toBeFalsy();
    expect(themeMock.markRequired).toHaveBeenCalledTimes(0);

    plugin.setRequired();

    expect(input.required).toBeTruthy();
    expect(plugin.getOptions().required).toBeTruthy();
    expect(themeMock.markRequired).toHaveBeenCalledTimes(1);
  });

  test('when already required', () => {
    const plugin = new Plugin(input, { required: true });

    expect(input.required).toBeTruthy();
    expect(plugin.getOptions().required).toBeTruthy();
    expect(themeMock.markRequired).toHaveBeenCalledTimes(1);

    plugin.setRequired();

    expect(input.required).toBeTruthy();
    expect(plugin.getOptions().required).toBeTruthy();
    expect(themeMock.markRequired).toHaveBeenCalledTimes(2);
  });
});

describe('unsetRequired', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;
  let input: HTMLInputElement;

  beforeAll(() => {
    input = document.createElement('input');
  });

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('when already not required', () => {
    const plugin = new Plugin(input, { required: false });

    expect(input.required).toBeFalsy();
    expect(plugin.getOptions().required).toBeFalsy();
    expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(1);

    plugin.unsetRequired();

    expect(input.required).toBeFalsy();
    expect(plugin.getOptions().required).toBeFalsy();
    expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(2);
  });

  test('when required', () => {
    const plugin = new Plugin(input, { required: true });

    expect(input.required).toBeTruthy();
    expect(plugin.getOptions().required).toBeTruthy();
    expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(0);

    plugin.unsetRequired();

    expect(input.required).toBeFalsy();
    expect(plugin.getOptions().required).toBeFalsy();
    expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(1);
  });
});

describe('enableLive', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;
  let input: HTMLInputElement;
  let event: Event;
  let tester: ITester;

  beforeAll(() => {
    event = new Event('input');
    tester = (_: any) => 'error';

    input = document.createElement('input');

    // Add value so that validation will run
    input.value = 'value';
  });

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('when not already live', () => {
    const plugin = new Plugin(input, {
      live: false,
      testers: [tester],
    });

    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);

    plugin.enableLive();
    expect(plugin.getOptions().live).toBeTruthy();

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
  });

  test('when already live', () => {
    const plugin = new Plugin(input, {
      live: true,
      testers: [tester],
    });

    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);

    plugin.enableLive();
    expect(plugin.getOptions().live).toBeTruthy();

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(2);

    // Check that event is only added once
    plugin.enableLive();
    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(3);
  });
});

describe('disableLive', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;
  let input: HTMLInputElement;
  let event: Event;
  let tester: ITester;

  beforeAll(() => {
    event = new Event('input');
    tester = (_: any) => 'error';

    input = document.createElement('input');

    // Add value so that validation will run
    input.value = 'value';
  });

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('when already not live', () => {
    const plugin = new Plugin(input, {
      live: false,
      testers: [tester],
    });

    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);

    plugin.disableLive();
    expect(plugin.getOptions().live).toBeFalsy();

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);
  });

  test('when already live', () => {
    const plugin = new Plugin(input, {
      live: true,
      testers: [tester],
    });

    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);

    plugin.disableLive();
    expect(plugin.getOptions().live).toBeFalsy();

    input.dispatchEvent(event);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
  });
});

describe('isValid', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;
  let input: HTMLInputElement;

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;

    input = document.createElement('input');
  });

  test('should trim input value', () => {
    const plugin = new Plugin(input);
    input.value = '   space padded string  ';

    expect(plugin.isValid()).toBeTruthy();
    expect(input.value).toBe('space padded string');
  });

  test('should mark missing, required input', () => {
    const plugin = new Plugin(input, { required: true });
    input.value = '   ';

    expect(themeMock.setMissing).toBeCalledTimes(0);
    expect(themeMock.clearErrors).toBeCalledTimes(0);

    expect(plugin.isValid()).toBeFalsy();

    expect(themeMock.setMissing).toBeCalledTimes(1);
    expect(themeMock.clearErrors).toBeCalledTimes(1);
    expect(input.value).toBe('');
  });

  test('should unmark missing, not-required input', () => {
    const plugin = new Plugin(input, { required: false });
    input.value = '   ';

    expect(themeMock.unsetMissing).toBeCalledTimes(0);
    expect(themeMock.clearErrors).toBeCalledTimes(0);

    expect(plugin.isValid()).toBeTruthy();

    expect(themeMock.unsetMissing).toBeCalledTimes(1);
    expect(themeMock.clearErrors).toBeCalledTimes(1);
    expect(input.value).toBe('');
  });

  test('should unmark non-missing input', () => {
    const plugin = new Plugin(input, {
      required: false,
      testers: [_ => 'error'],
    });
    input.value = 'value';

    expect(themeMock.unsetMissing).toBeCalledTimes(0);
    expect(themeMock.clearErrors).toBeCalledTimes(0);
    expect(themeMock.setErrors).toBeCalledTimes(0);

    expect(plugin.isValid()).toBeFalsy();

    expect(themeMock.unsetMissing).toBeCalledTimes(1);
    expect(themeMock.clearErrors).toBeCalledTimes(0);
    expect(themeMock.setErrors).toBeCalledTimes(1);
    expect(themeMock.setErrors).toBeCalledWith(['error']);
  });
});

describe('blur event', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;
  let input: HTMLInputElement;
  let blurEvent: Event;

  beforeAll(() => {
    blurEvent = new Event('blur');
  });

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;

    input = document.createElement('input');
  });

  test('should trim input value', () => {
    const plugin = new Plugin(input);
    input.value = '   space padded string  ';

    input.dispatchEvent(blurEvent);

    expect(input.value).toBe('space padded string');
  });

  test('should mark missing, required input', () => {
    const plugin = new Plugin(input, { required: true });
    input.value = '   ';

    expect(themeMock.setMissing).toBeCalledTimes(0);
    expect(themeMock.clearErrors).toBeCalledTimes(0);

    input.dispatchEvent(blurEvent);

    expect(themeMock.setMissing).toBeCalledTimes(1);
    expect(themeMock.clearErrors).toBeCalledTimes(1);
  });

  test('should unmark missing, not-required input', () => {
    const plugin = new Plugin(input, { required: false });
    input.value = '   ';

    expect(themeMock.unsetMissing).toBeCalledTimes(0);
    expect(themeMock.clearErrors).toBeCalledTimes(0);

    input.dispatchEvent(blurEvent);

    expect(themeMock.unsetMissing).toBeCalledTimes(1);
    expect(themeMock.clearErrors).toBeCalledTimes(1);
  });

  test('should unmark non-missing input', () => {
    const plugin = new Plugin(input, {
      required: false,
      testers: [_ => 'error'],
    });
    input.value = 'value';

    expect(themeMock.unsetMissing).toBeCalledTimes(0);
    expect(themeMock.clearErrors).toBeCalledTimes(0);
    expect(themeMock.setErrors).toBeCalledTimes(0);

    input.dispatchEvent(blurEvent);

    expect(themeMock.unsetMissing).toBeCalledTimes(1);
    expect(themeMock.clearErrors).toBeCalledTimes(0);
    expect(themeMock.setErrors).toBeCalledTimes(1);
    expect(themeMock.setErrors).toBeCalledWith(['error']);
  });
});

describe('input event', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;
  let input: HTMLInputElement;
  let plugin: Plugin;
  let tester: ITester;
  let inputEvent: Event;

  beforeAll(() => {
    inputEvent = new Event('input');

    tester = value => {
      if (value !== 'a') {
        return 'not first letter of alphabet';
      }
    };
  });

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    input = document.createElement('input');

    Plugin.defaults.theme = themeConstructor;

    plugin = new Plugin(input, { live: true, testers: [tester] });
  });

  test('should clear errors when valid', () => {
    expect(themeMock.clearErrors).toHaveBeenCalledTimes(0);
    input.value = 'a';

    input.dispatchEvent(inputEvent);

    expect(themeMock.clearErrors).toHaveBeenCalledTimes(1);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);
  });

  test('should set errors when invalid', () => {
    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);
    input.value = 'b';

    input.dispatchEvent(inputEvent);

    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
    expect(themeMock.setErrors).toHaveBeenCalledWith(['not first letter of alphabet']);
    expect(themeMock.clearErrors).toHaveBeenCalledTimes(0);
  });

  test('should not set errors when empty', () => {
    expect(themeMock.clearErrors).toHaveBeenCalledTimes(0);
    input.value = '';

    input.dispatchEvent(inputEvent);

    expect(themeMock.clearErrors).toHaveBeenCalledTimes(1);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);
  });
});

describe('destroy', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('removes events', () => {
    const blurEvent = new Event('blur');
    const inputEvent = new Event('input');
    const input = document.createElement('input');

    const plugin = new Plugin(input, {
      live: true,
      required: true,
      testers: [_ => 'error'],
    });

    expect(themeMock.setErrors).toHaveBeenCalledTimes(0);
    expect(themeMock.setMissing).toHaveBeenCalledTimes(0);

    input.value = 'value';
    input.dispatchEvent(inputEvent);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
    expect(themeMock.setMissing).toHaveBeenCalledTimes(0);

    input.value = '';
    input.dispatchEvent(blurEvent);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
    expect(themeMock.setMissing).toHaveBeenCalledTimes(1);

    plugin.destroy();

    input.value = 'value';
    input.dispatchEvent(inputEvent);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
    expect(themeMock.setMissing).toHaveBeenCalledTimes(1);

    input.value = '';
    input.dispatchEvent(blurEvent);
    expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
    expect(themeMock.setMissing).toHaveBeenCalledTimes(1);
  });

  test('resets input / theme', () => {
    const plugin = new Plugin(document.createElement('input'));

    expect(themeMock.unsetMissing).toHaveBeenCalledTimes(0);
    expect(themeMock.clearErrors).toHaveBeenCalledTimes(0);

    plugin.destroy();

    expect(themeMock.unsetMissing).toHaveBeenCalledTimes(1);
    expect(themeMock.clearErrors).toHaveBeenCalledTimes(1);
  });
});
