import { ITester } from 'livevalidator';
import { IOptions } from './abstractions';
import { Plugin } from './plugin';

/**
 * Adds LiveValidator to [[HTMLInputElement]]s
 */
export interface IPluginHTMLInputElement extends HTMLInputElement {
  LiveValidator: Plugin;
}

/**
 * Handles the [[Plugin]] on a collection of [[HTMLInputElement]]s
 */
export class PluginCollection {
  /**
   * Filter used to only find valid inputs
   */
  private validInputsFilter =
    'input:not([type="button"])' +
    ':not([type="file"])' +
    ':not([type="hidden"])' +
    ':not([type="image"])' +
    ':not([type="radio"])' +
    ':not([type="reset"])' +
    ':not([type="submit"])' +
    ', textarea';

  /**
   * List of [[HTMLInputElement]]s being handled
   */
  private elements: IPluginHTMLInputElement[];

  /**
   * Creates the [[Plugin]] on a set of input elements
   * @param options Options to instantiate [[Plugin]] with
   * @param elements Elements that will be searched for valid inputs the [[Plugin]] can operate on
   */
  constructor(options: IOptions, ...elements: HTMLElement[]) {
    // Filter out inputs from elements
    this.elements = elements
      .filter(element => element.matches(this.validInputsFilter))
      .map(element => element as IPluginHTMLInputElement);

    // Find input children in each element
    elements.forEach(element =>
      element.querySelectorAll(this.validInputsFilter).forEach(e => this.elements.push(e as IPluginHTMLInputElement)),
    );

    // Instantiate plugin
    this.elements.forEach(element => (element.LiveValidator = new Plugin(element, options)));
  }

  /**
   * Gets all the elements this collection is handling
   * @returns Elements being handled
   */
  public getElements(): HTMLElement[] {
    return this.elements;
  }

  /**
   * Disables lives on all the elements being handled
   */
  public disableLive() {
    this.elements.forEach(e => e.LiveValidator.disableLive());
  }

  /**
   * Enables lives on all the elements being handled
   */
  public enableLive() {
    this.elements.forEach(e => e.LiveValidator.enableLive());
  }

  /**
   * Sets all the elements being handled as required
   */
  public setRequired() {
    this.elements.forEach(e => e.LiveValidator.setRequired());
  }

  /**
   * Sets all the elements being handled as non-required
   */
  public unsetRequired() {
    this.elements.forEach(e => e.LiveValidator.unsetRequired());
  }

  /**
   * Check if all the elements being handled are valid
   * @returns True iif all are valid
   */
  public isValid(): boolean {
    // Assume valid to start
    let valid = true;

    this.elements.forEach(e => {
      if (!e.LiveValidator.isValid()) {
        valid = false;
      }
    });

    return valid;
  }

  /**
   * Destroy the plugin all the elements being handled
   */
  public destroy() {
    this.elements.forEach(e => {
      e.LiveValidator.destroy();
      delete e.LiveValidator;
    });
  }

  /**
   * Adds testers to all the elements being handled
   * @param testers Testers to add
   */
  public addTesters(...testers: ITester[]) {
    this.elements.forEach(e => e.LiveValidator.addTesters(...testers));
  }

  /**
   * Removes testers from all the elements being handled
   * @param testers Testers to remove
   */
  public removeTesters(...testers: ITester[]) {
    this.elements.forEach(e => e.LiveValidator.removeTesters(...testers));
  }

  /**
   * Removes all the testers from all the elements being handled
   */
  public removeAllTesters() {
    this.elements.forEach(e => e.LiveValidator.removeAllTesters());
  }

  /**
   * Change the locale of all the elements being handled
   * @param locale Locale to set
   */
  public setLocale(locale: string) {
    this.elements.forEach(e => e.LiveValidator.setLocale(locale));
  }
}
