import { Core } from 'livevalidator';
import { IInnerOptions, IOptions, ITheme } from './abstractions';

/**
 * DOM plugin wrapper for LiveValidator's core
 */
export class Plugin extends Core {
  /**
   * Global defaults that can be changed using `Plugin.defaults`
   */
  public static defaults: IOptions = {
    live: true,
    locale: 'en-US',
    required: false,
    testers: [],
  };

  /**
   * Options that are kept track of internally
   */
  private options: IInnerOptions;

  /**
   * Instance of theme to manipulate input
   */
  private theme: ITheme;

  /**
   * Is the input missing a value (and required)
   */
  private missing: boolean;

  /**
   * Constructs a new plugin on the given input
   * @param element The HTML input plugin will act on
   * @param options Options to configure plugin
   */
  constructor(private element: HTMLInputElement, options: IOptions = {}) {
    super();

    // Combine with defaults and split inner from testers and theme
    const { testers, theme, ...inner } = { ...Plugin.defaults, ...options };
    this.options = {
      live: !!inner.live,
      locale: inner.locale == null ? 'en-US' : inner.locale,
      required: !!inner.required,
    };

    // Construct theme
    if (null == theme) {
      throw new Error('A theme constructor is required');
    }
    this.theme = theme(this.element);

    if (null != testers) {
      this.addTesters(...testers);
    }

    // Update states
    this.options.required ? this.setRequired() : this.unsetRequired();
    this.options.live ? this.enableLive() : this.disableLive();

    this.missing = false;
    this.element.addEventListener('blur', this.blurEvent);
  }

  /**
   * Allows retrieval of options for debugging
   */
  public getOptions(): IInnerOptions {
    return this.options;
  }

  /**
   * Set the input as required
   */
  public setRequired() {
    this.element.required = this.options.required = true;

    this.theme.markRequired();
  }

  /**
   * Make the input non-required
   */
  public unsetRequired() {
    this.element.required = this.options.required = false;

    this.theme.unmarkRequired();
  }

  /**
   * Enable live validation on the input
   */
  public enableLive() {
    this.options.live = true;
    this.element.addEventListener('input', this.inputEvent);
  }

  /**
   * Disable live validation on the input
   */
  public disableLive() {
    this.options.live = false;
    this.element.removeEventListener('input', this.inputEvent);
  }

  /**
   * Check if the input's current state is valid
   * @returns True if it is valid
   */
  public isValid(): boolean {
    this.blurEvent();

    return !this.missing && 0 === this.getErrors().length;
  }

  /**
   * Destroys this instance of the plugin
   * Resets the input to normal
   */
  public destroy() {
    this.element.removeEventListener('input', this.inputEvent);
    this.element.removeEventListener('blur', this.blurEvent);

    this.theme.clearErrors();
    this.theme.unsetMissing();
  }

  /**
   * Runs when the input looses focus
   */
  private blurEvent = () => {
    // Trim the input for spaces
    const value = this.element.value.trim();

    if (value !== this.element.value) {
      this.element.value = value;
    }

    // Assume not missing
    this.missing = false;

    // If value is empty
    if (!value) {
      if (this.options.required) {
        this.missing = true;
        this.theme.setMissing();
      } else {
        this.theme.unsetMissing();
      }

      this.theme.clearErrors();
    } else {
      this.theme.unsetMissing();
      this.inputEvent();
    }
  };

  /**
   * Runs when the input is changes and `live` is enabled
   */
  private inputEvent = () => {
    const value = this.element.value;

    if (!value || this.validate(value)) {
      this.theme.clearErrors();
    } else {
      this.theme.setErrors(this.getErrors());
    }
  };
}
