import { ITester } from 'livevalidator';

/**
 * Interface for DOM themes
 */
export interface ITheme {
  /**
   * Mark the input as required visually
   */
  markRequired(): void;

  /**
   * Unmarks the input as required visually
   */
  unmarkRequired(): void;

  /**
   * Marks the input as missing visually
   */
  setMissing(): void;

  /**
   * Unmarks the input as missing visually
   */
  unsetMissing(): void;

  /**
   * Clears all visual errors on the input
   */
  clearErrors(): void;

  /**
   * Sets visual errors on the input
   * @param errors The errors to set
   */
  setErrors(errors: string[]): void;
}

/**
 * Functions that can construct a [[ITheme]] on an input
 * @param element The input element this theme will act on
 * @returns A theme
 */
export type TThemeConstructable = (element: HTMLInputElement) => ITheme;

/**
 * Options stored inside the plugin
 */
export interface IInnerOptions {
  live: boolean;
  locale: string;
  required: boolean;
}

/**
 * Set of options that can be passed to the plugin
 */
export interface IOptions {
  /**
   * Should validation be done on each input change
   */
  live?: boolean;

  /**
   * Locale to use for errors
   */
  locale?: string;

  /**
   * Is the input required
   */
  required?: boolean;

  /**
   * The testers that should be run against the input
   */
  testers?: ITester[];

  /**
   * The theme to use
   */
  theme?: TThemeConstructable;
}
