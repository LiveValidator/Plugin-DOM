import { ITheme, TThemeConstructable } from './abstractions';
import { IPluginHTMLInputElement, PluginCollection } from './collection';
import { Plugin } from './plugin';

/**
 * Helper to get a mock theme for tests
 */
function getThemeMock(): ITheme {
  return {
    clearErrors: jest.fn(),
    markRequired: jest.fn(),
    setErrors: jest.fn(),
    setMissing: jest.fn(),
    unmarkRequired: jest.fn(),
    unsetMissing: jest.fn(),
  };
}

/**
 * Helper to register a theme with a constructor
 * @param theme Theme to register
 */
function getThemeConstructor(theme: ITheme): TThemeConstructable {
  return jest.fn(_ => theme);
}

describe('Constructor', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('on form to extract supported inputs', () => {
    const form = document.createElement('form');
    form.innerHTML = `
            <input />
            <input type="button">
            <input type="checkbox">
            <input type="color">
            <input type="date">
            <input type="datetime-local">
            <input type="email">
            <input type="file">
            <input type="hidden">
            <input type="image">
            <input type="month">
            <input type="number">
            <input type="password">
            <input type="radio">
            <input type="range">
            <input type="reset">
            <input type="search">
            <input type="submit">
            <input type="tel">
            <input type="text">
            <input type="time">
            <input type="url">
            <input type="week">
            <textarea></textarea>
        `;

    expect(new PluginCollection({}, form).getElements()).toMatchSnapshot();
  });

  test('on element to extract supported inputs', () => {
    const div = document.createElement('div');
    div.innerHTML = `
        <div>
            <input />
            <input type="button">
            <input type="checkbox">
            <input type="color">

            <form>
                <input type="date">
                <input type="datetime-local">
                <input type="email">
                <input type="file">
                <input type="hidden">
                <input type="image">
                <input type="month">
            </form>
            <input type="number">
            <input type="password">
        </div>
            <input type="radio">
            <input type="range">
        <form>
            <input type="reset">
            <input type="search">
            <input type="submit">
            <div>
                <input type="tel">
                <input type="text">
                <input type="time">
                <input type="url">
                <input type="week">
                <textarea></textarea>
            </div>
        </form>
        `;

    expect(new PluginCollection({}, div).getElements()).toMatchSnapshot();
  });
});

test('disableLive', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const event = new Event('input');
  const collection = new PluginCollection({ theme: themeConstructor, live: true, testers: [_ => 'e'] }, input);

  input.value = 'value';

  input.dispatchEvent(event);
  expect(themeMock.setErrors).toHaveBeenCalledTimes(1);

  collection.disableLive();

  input.dispatchEvent(event);
  expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
});

test('enableLive', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const event = new Event('input');
  const collection = new PluginCollection({ theme: themeConstructor, live: false, testers: [_ => 'e'] }, input);

  input.value = 'value';

  input.dispatchEvent(event);
  expect(themeMock.setErrors).toHaveBeenCalledTimes(0);

  collection.enableLive();

  input.dispatchEvent(event);
  expect(themeMock.setErrors).toHaveBeenCalledTimes(1);
});

test('setRequired', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const collection = new PluginCollection({ theme: themeConstructor, required: false }, input);

  expect(themeMock.markRequired).toHaveBeenCalledTimes(0);

  collection.setRequired();

  expect(themeMock.markRequired).toHaveBeenCalledTimes(1);
});

test('unsetRequired', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const collection = new PluginCollection({ theme: themeConstructor, required: true }, input);

  expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(0);

  collection.unsetRequired();

  expect(themeMock.unmarkRequired).toHaveBeenCalledTimes(1);
});

describe('isValid', () => {
  let themeMock: ITheme;
  let themeConstructor: TThemeConstructable;

  beforeEach(() => {
    themeMock = getThemeMock();
    themeConstructor = getThemeConstructor(themeMock);

    Plugin.defaults.theme = themeConstructor;
  });

  test('when all is valid', () => {
    const input1: HTMLInputElement = document.createElement('input');
    const input2: HTMLInputElement = document.createElement('input');
    const input3: HTMLInputElement = document.createElement('input');
    const collection: PluginCollection = new PluginCollection(
      {
        testers: [
          _ => {
            return;
          },
        ],
      },
      input1,
      input2,
      input3,
    );

    input1.value = input2.value = input3.value = 'value';

    expect(collection.isValid()).toBeTruthy();
    expect(themeMock.setErrors).toBeCalledTimes(0);
  });

  test('when all is invalid', () => {
    const input1: HTMLInputElement = document.createElement('input');
    const input2: HTMLInputElement = document.createElement('input');
    const input3: HTMLInputElement = document.createElement('input');
    const collection: PluginCollection = new PluginCollection({ testers: [_ => 'error'] }, input1, input2, input3);

    input1.value = input2.value = input3.value = 'value';

    expect(collection.isValid()).toBeFalsy();
    expect(themeMock.setErrors).toBeCalledTimes(3);
  });

  test('when one is invalid', () => {
    const input1: HTMLInputElement = document.createElement('input');
    const input2: HTMLInputElement = document.createElement('input');
    const input3: HTMLInputElement = document.createElement('input');
    const collection: PluginCollection = new PluginCollection(
      { testers: [value => (value !== 'a' ? 'Not a' : undefined)] },
      input1,
      input2,
      input3,
    );

    input1.value = input3.value = 'a';
    input2.value = 'b';

    expect(collection.isValid()).toBeFalsy();
    expect(themeMock.setErrors).toBeCalledTimes(1);
  });
});

test('destroy', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input') as IPluginHTMLInputElement;
  const collection = new PluginCollection({ theme: themeConstructor }, input);

  expect(input.LiveValidator).toBeDefined();
  expect(themeMock.unsetMissing).toHaveBeenCalledTimes(0);
  expect(themeMock.clearErrors).toHaveBeenCalledTimes(0);

  collection.destroy();

  expect(input.LiveValidator).not.toBeDefined();
  expect(themeMock.unsetMissing).toHaveBeenCalledTimes(1);
  expect(themeMock.clearErrors).toHaveBeenCalledTimes(1);
});

test('addTesters', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const collection = new PluginCollection({ theme: themeConstructor }, input);
  const tester = jest.fn(_ => 'error');

  input.value = 'value';

  collection.addTesters(tester);
  collection.isValid();

  expect(themeMock.setErrors).toBeCalledTimes(1);
  expect(tester).toBeCalledTimes(1);
});

test('removeTesters', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const tester = jest.fn(_ => 'error');
  const collection = new PluginCollection({ theme: themeConstructor, testers: [tester] }, input);

  input.value = 'value';

  collection.isValid();

  expect(themeMock.setErrors).toBeCalledTimes(1);
  expect(tester).toBeCalledTimes(1);

  collection.removeTesters(tester);

  collection.isValid();

  expect(themeMock.setErrors).toBeCalledTimes(1);
  expect(tester).toBeCalledTimes(1);
});

test('removeAllTesters', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const tester = jest.fn(_ => 'error');
  const collection = new PluginCollection({ theme: themeConstructor, testers: [tester] }, input);

  input.value = 'value';

  collection.isValid();

  expect(themeMock.setErrors).toBeCalledTimes(1);
  expect(tester).toBeCalledTimes(1);

  collection.removeAllTesters();

  collection.isValid();

  expect(themeMock.setErrors).toBeCalledTimes(1);
  expect(tester).toBeCalledTimes(1);
});

test('setLocale', () => {
  const themeMock = getThemeMock();
  const themeConstructor = getThemeConstructor(themeMock);
  const input = document.createElement('input');
  const tester = jest.fn(_ => 'error');
  const collection = new PluginCollection({ theme: themeConstructor, testers: [tester] }, input);

  input.value = 'value';

  collection.isValid();

  expect(tester).toBeCalledTimes(1);
  expect(tester).toBeCalledWith('value', 'en-US');
  expect(tester).not.toBeCalledWith('value', 'en-ZA');

  collection.setLocale('en-ZA');

  collection.isValid();

  expect(tester).toBeCalledTimes(2);
  expect(tester).toBeCalledWith('value', 'en-ZA');
});
